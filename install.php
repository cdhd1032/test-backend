<?php

if (isset($_POST['DBHOST'])) {
    // print_r($_POST);

    $servername = $_POST['DBHOST'];
    $username = $_POST['DBUSER'];
    $password = $_POST['DBPASS'];

    $informe = "";
    $estado = 'error';

    try {
        //code...
        //Crear Conexion con MYSQL
        $conn = new mysqli($servername, $username, $password);
        //Comprobar la Conexión
        if ($conn->connect_error) {
            echo "<script>window.location='install.php?error'</script>";
            // die("Fallo de Conexión: " . $conn->connect_error);
        }
        //Crear base de datos
        $sql = "CREATE DATABASE IF NOT EXISTS " . $_POST['DBNAME'];
        if ($conn->query($sql) === TRUE) {
            $informe .= "Base de Datos {$_POST['DBNAME']} Creada.<br>";
        } else {
            echo "Error al Crear la Base de Datos:" . $conn->error;
        }


        // Seleccionar base de datos
        $conn->select_db($_POST['DBNAME']);

        $conn->query("DROP TABLE IF EXISTS peticiones");
        $conn->query("DROP TABLE IF EXISTS usuarios");



        //Crear tablas
        $sql = "CREATE TABLE `peticiones` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `url` varchar(500) DEFAULT NULL,
                `respuesta` text DEFAULT NULL,
                `nombre_peticion` varchar(45) DEFAULT NULL,
                `created_at` timestamp NULL DEFAULT NULL,
                `updated_at` timestamp NULL DEFAULT NULL,
                PRIMARY KEY (`id`)
              ) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
              ";

        if ($conn->query($sql) === TRUE) {
            $informe .= "Tabla peticiones Creada.<br>";

            //Crear tablas
            $sql = "CREATE TABLE `usuarios` (
                    `id` int(11) NOT NULL AUTO_INCREMENT,
                    `documento` varchar(15) NOT NULL,
                    `email` varchar(150) NOT NULL,
                    `nombre` varchar(150) NOT NULL,
                    `pais` varchar(150) NOT NULL,
                    `password` text NOT NULL,
                    `created_at` timestamp NULL DEFAULT NULL,
                    `updated_at` timestamp NULL DEFAULT NULL,
                    PRIMARY KEY (`id`),
                    UNIQUE KEY `usuario_documento` (`documento`),
                    UNIQUE KEY `usuario_email` (`email`)
                  ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
                  ";

            if ($conn->query($sql) === TRUE) {
                $informe .= "Tabla usuarios Creada.<br>";
                $estado = 'ok';
            } else {
                echo "Error al Crear la Base de Datos:" . $conn->error;
            }
        } else {
            echo "Error al Crear la Base de Datos:" . $conn->error;
        }

        if($estado == 'ok'){
            if ( rename("./app/config/config.php", "./app/config/config_old.php") ){
                $informe .= "Archivo config renombrado.<br>";
            }

            $fh = fopen("./app/config/config.php", 'w') or die("Se produjo un error al crear el archivo");
  
                $texto = <<<_END
                <?php
                defined("DBDRIVER")or define('DBDRIVER','mysql'); 
                defined("DBHOST")or define('DBHOST','{$_POST["DBHOST"]}'); 
                defined("DBNAME")or define('DBNAME','{$_POST["DBNAME"]}'); 
                defined("DBUSER")or define('DBUSER','{$_POST["DBUSER"]}');
                defined("DBPASS")or define('DBPASS','{$_POST["DBPASS"]}');
                _END;
                
                fwrite($fh, $texto) or die("No se pudo escribir en el archivo");
                
                fclose($fh);
                
                echo "Se ha escrito sin problemas";

        }

        //Cerrar Conexión
        $conn->close();
    } catch (\Throwable $th) {
        //throw $th;
        echo $th;
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>Document</title>
</head>

<body>

    <div class="container">

        <?php
        if (isset($_GET['error'])) {
        ?>
            <div class="alert alert-danger">
                Fallo conexión por favor verifique los datos ingresados, tenga en cuenta que el usuario y clave deben existe en el gestor de base de datos
            </div>
        <?php
        }
        ?>

        <?php
        if (isset($estado) and $estado == 'ok') {
        ?>
            <div class="alert alert-success">
                La configuración se realizó correctamente clic <a href="login">aquí</a> para continuar
            </div>
        <?php
        } else {

        ?>
            <h3>Ingrese la siguiente información para configurar la aplicación</h3>

            <form action="install.php" method="post">
                <div class="form-group">
                    <label for="">Host de la base de datos</label>
                    <input type="text" class="form-control" name="DBHOST" id="DBHOST" value="<?php echo (isset($_POST['DBHOST'])) ?  $_POST['DBHOST'] : 'localhost'; ?>" required>
                    <!-- <small id="helpId" class="form-text text-muted">Help text</small> -->
                </div>

                <div class="form-group">
                    <label for="">Nombre de la base de datos <small>No es necesario que la base exista</small></label>
                    <input type="text" class="form-control" name="DBNAME" id="DBNAME" required value="<?php echo (isset($_POST['DBNAME'])) ?  $_POST['DBNAME'] : ''; ?>">
                    <!-- <small id="helpId" class="form-text text-muted">Help text</small> -->
                </div>

                <div class="form-group">
                    <label for="">Usuario base de datos <small>El usuario debe existir</small></label>
                    <input type="text" class="form-control" name="DBUSER" id="DBUSER" required value="<?php echo (isset($_POST['DBUSER'])) ?  $_POST['DBUSER'] : ''; ?>">
                    <!-- <small id="helpId" class="form-text text-muted">Help text</small> -->
                </div>

                <div class="form-group">
                    <label for="">Clave base de datos</label>
                    <input type="text" class="form-control" name="DBPASS" id="DBPASS">
                    <!-- <small id="helpId" class="form-text text-muted">Help text</small> -->
                </div>
                <button type="submit" name="" id="" class="btn btn-primary">Guardar configuración</button>
            </form>
        <?php

        }
        ?>




    </div>



</body>

</html>