<?php

ini_set('display_errors', false);

session_start();

require 'app/config/config.php';
require 'vendor/autoload.php';

use Clase\CustomerData;
use Controllers\UsuarioController;
use Database\Database;
use Helpers\Alertas;


// Validar base de datos
if( !mysqli_connect(DBHOST, DBUSER, DBPASS, DBNAME) ){
    header('location:install.php');
}


//Initialize Illuminate Database Connection
new Database;

$alerta = new Alertas;

if (isset($_POST["accion"])) {
    switch ($_POST["accion"]) {

        case 'REGISTER':
            # code...
            // print_r($_POST);
            $data = [
                'documento' => $_POST["documento"],
                'email'     => $_POST["email"],
                'nombre'    => $_POST["nombre"],
                'pais'      => $_POST["pais"]
            ];
            $user = UsuarioController::createUser($data,  $_POST["clave"]);
            // print_r($user);
            if ($user["ok"] == 'false') {
                echo $alerta->alertaDiv('danger', 'Error de registro', $user["msg"]);
            } else {
                $user = UsuarioController::loginUser($_POST["documento"], $_POST["clave"]);
                echo "<script> window.location='inicio'; </script>";
            }

            break;


        case 'LOGIN':
            # code...
            $user = UsuarioController::loginUser($_POST["documento"], $_POST["clave"]);
            // print_r($user);
            if ($user["ok"] == 'false') {
                echo $alerta->alertaDiv('danger', 'Error de acceso', $user["msg"]);
            } else {
                echo "<script> window.location='inicio'; </script>";
            }

            break;


        case 'LOGOUT':
            # code...
            session_unset();
            session_destroy();

            echo "<script> window.location='login'; </script>";

            break;


        case 'EXTERNO':
            # code...
            // print_r($_POST);
            $dataObject = new CustomerData($_POST["method"], $_POST["params"], $_POST["tipo"]);
            $datos = $dataObject->data;
            echo $datos;

            break;

        default:
            # code...
            break;
    }
}



// Registro

// $data = [
//     'documento' => '123456789',
//     'email'     => 'pruebafg@gmail.com',
//     'nombre'    => 'Carlos',
//     'pais'      => 'Colombia'
// ];
// $user = UsuarioController::createUser($data, '123456789');
// print_r($user);



// Login

// $user = UsuarioController::loginUser('1234567890', '123456789');
// print_r($user);



// Fuentes externas

// Retorna json
// $dataObject = new CustomerData('http://www.mocky.io/v2/5d9f38fd3000005b005246ac?mocky-delay=2s', 'GET', array());
// $datos = $dataObject->data;
// print_r($datos);
