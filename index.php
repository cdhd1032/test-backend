<?php

require 'start.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="./public/src/css/style.css">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">
</head>
<body>

<div class="container" id="contenido">
    <?php
        //Middlewares
        if ( isset($_SESSION["global_nombre"]) and strlen($_SESSION["global_nombre"]) > 0 ){
            require './app/views/inicio.php';
        }else{
            $vista = (isset($_GET["view"]))? $_GET["view"] : 'login';
            require "./app/views/{$vista}.php";
        }
    ?>
</div>

<script src="https://code.jquery.com/jquery-2.2.2.min.js"></script>
<script src="//cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="./public/src/js/main.js"></script>
    
</body>
</html>