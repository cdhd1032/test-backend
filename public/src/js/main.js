// Carga vista
function cargarVista(view) {
  localStorage.setItem("view", view);
  // document.getElementById(idRender).innerHTML = view;
  window.location = view;
}

$(document).ready(function () {
  // Inicialización datatables
  $(".datatable").DataTable();

  // Ajax formulario
  $("#formulario").bind("submit", function () {
    // Capturamnos el boton de envío
    var btnEnviar = $("#btnEnviar");
    $.ajax({
      type: $(this).attr("method"),
      url: $(this).attr("action"),
      data: $(this).serialize(),
      beforeSend: function () {
        /*
         * Esta función se ejecuta durante el envió de la petición al
         * servidor.
         * */
        // btnEnviar.text("Enviando"); Para button
        btnEnviar.val("Enviando"); // Para input de tipo button
        btnEnviar.attr("disabled", "disabled");
      },
      complete: function (data) {
        /*
         * Se ejecuta al termino de la petición
         * */
        btnEnviar.val("Enviar formulario");
        btnEnviar.removeAttr("disabled");
      },
      success: function (data) {
        /*
         * Se ejecuta cuando termina la petición y esta ha sido
         * correcta
         * */
        $(".respuesta").html(data);
      },
      error: function (data) {
        /*
         * Se ejecuta si la peticón ha sido erronea
         * */
        alert("Problemas al tratar de enviar el formulario");
      },
    });
    // Nos permite cancelar el envio del formulario
    return false;
  });
});

function buscarInformacion() {
  // Declare variables
  var input, filter, table, tr, td, i, j, visible;
  input = document.getElementById("buscador");
  filter = input.value.toUpperCase();
  table = document.getElementById("usuarios");
  tr = table.getElementsByTagName("tr");

  // console.log(filter);

  for (i = 1; i < tr.length; i++) {
    visible = false;

    td = tr[i].getElementsByTagName("td");

    // Las pocisiones 2, 3, 4 Estan relacionadas con las columnas correo, primer nombre y apellido respectivamente

    for (j = 2; j < 5; j++) {
      if (td[j] && td[j].innerHTML.toUpperCase().indexOf(filter) > -1) {
        visible = true;
      }
    }
    if (visible === true) {
      tr[i].style.display = "";
    } else {
      tr[i].style.display = "none";
    }
  }
}

