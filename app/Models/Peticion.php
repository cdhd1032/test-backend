<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Peticion extends Model
{
    protected $table = 'peticiones';
    protected $fillable = ['url', 'respuesta', 'nombre_peticion'];
}
