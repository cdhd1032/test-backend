<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model{

    protected $table = 'usuarios';
    protected $fillable = ['documento','email','nombre','pais','password'];

}