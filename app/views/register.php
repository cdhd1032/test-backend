<div class="card col-md-4 m-auto mt-4">
    <div class="card-header">
        Digita la siguiente información para ingresar a la base de datos
    </div>

    <div class="card-body">
        <form action="start.php" method="post" id="formulario">
            <div class="form-group">
              <label for="">Nombre</label>
              <input type="text"
                class="form-control" name="nombre" id="nombre">
            </div>
            <div class="form-group">
              <label for="">Documento</label>
              <input type="text"
                class="form-control" name="documento" id="documento">
            </div>
            <div class="form-group">
              <label for="">Pais</label>
              <select
                class="form-control" name="pais" id="pais"></select>
            </div>
            <div class="form-group">
              <label for="">Correo</label>
              <input type="text"
                class="form-control" name="email" id="email">
            </div>
            <div class="form-group">
              <label for="">Clave</label>
              <input type="password"
                class="form-control" name="clave" id="clave" placeholder="*******">
              <!-- <small id="helpId" class="form-text text-muted">Help text</small> -->
            </div><br>
            <input type="hidden" name="accion" value="REGISTER">
            <button type="submit" name="" id="btnEnviar" class="btn btn-primary">Guardar</button>
            <hr>
            <i class="text-lightgray">¿Ya cuentas con un usuario y clave de acceso?, ingresa <a href="javascript:void()" onclick="cargarVista('login')">aquí</a></i>
        </form>
        <div class="respuesta"></div>
    </div>
</div>

<script>
    window.onload = function() {

        const params = [];

        const data = new URLSearchParams("tipo=PAISES&accion=EXTERNO&method=GET&");
        data.append('params', params);

        fetch('start.php', {
                method: 'POST',
                body: data
            })
            .then(function(response) {
                if (response.ok) {
                    return response.text()
                } else {
                    throw "Error en la llamada Ajax";
                }

            })
            .then(function(texto) {
                let dataJson = JSON.parse(texto);
                // console.log(dataJson);

                for (let index = 0; index < dataJson.length; index++) {
                    const element = dataJson[index];
                    // console.log(element.name);

                    document.getElementById('pais').innerHTML += '<option value="' + element.name + '">' + element.name + '</option>';
                }

            })
            .catch(function(err) {
                console.log(err);
            });


    };
</script>