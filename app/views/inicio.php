<?php

if (!isset($_SESSION['global_nombre'])) {
    header('location:login');
}

?>

<div class="cargando">

    <img src="./public/src/img/loader.gif" class="m-auto" alt="">
    <h4>Cargando usuarios</h4>

</div>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <!-- <a class="navbar-brand" href="#">Navbar</a> -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">Usuarios</a>
            </li>
        </ul>
        <form method="post" action="start.php" id="formulario">
            <input type="hidden" name="accion" value="LOGOUT">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Salir</button>
            <div class="respuesta"></div>
        </form>
    </div>
</nav>

<h2>Bienvenido <?php echo $_SESSION['global_nombre']; ?></h2>

<hr>

<!-- <form action="" method="post"> -->
<div class="input-group mb-3">
    <input type="text" class="form-control" placeholder="Puede buscar por nombre o correo electrónico del usuario" id="buscador" onkeyup="buscarInformacion()">
    <!-- <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="button">Buscar</button>
        </div> -->
</div>
<!-- </form> -->
<!-- class="datatable display"  -->
<table class="table table-bordered" id="usuarios" style="width:100%">
    <thead>
        <tr>
            <th>Id</th>
            <th>Cargo</th>
            <th>Correo</th>
            <th>Primer Nombre</th>
            <th>Apellido</th>
            <th>Cédula</th>
            <th>Teléfono</th>
            <th>País</th>
            <th>Departamento</th>
            <th>Ciudad</th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>


<script>
    window.onload = function() {

        $('.cargando').show();

        const params = [];

        const data = new URLSearchParams("tipo=USUARIOS&accion=EXTERNO&method=GET&");
        data.append('params', params);

        fetch('start.php', {
                method: 'POST',
                body: data
            })
            .then(function(response) {
                if (response.ok) {
                    return response.text()
                } else {
                    throw "Error en la llamada Ajax";
                }

            })
            .then(function(texto) {
                let dataJson = JSON.parse(texto);
                // console.log(dataJson['objects']);
                // console.log(dataJson['objects'].length);

                for (let index = 0; index < dataJson['objects'].length; index++) {
                    const element = dataJson['objects'][index];
                    console.log(element);
                    document.getElementById("usuarios").insertRow(index + 1).innerHTML = `<td>${element['id']}</td>
                                                                                          <td>${element['cargo']}</td>
                                                                                          <td>${element['correo']}</td>
                                                                                          <td>${element['primer_nombre']}</td>
                                                                                          <td>${element['apellido']}</td>
                                                                                          <td>${element['cedula']}</td>
                                                                                          <td>${element['telefono']}</td>
                                                                                          <td>${element['pais']}</td>
                                                                                          <td>${element['departamento']}</td>
                                                                                          <td>${element['ciudad']}</td>`;
                }

                $('.cargando').hide();

            })
            .catch(function(err) {
                console.log(err);
            });


    };
</script>