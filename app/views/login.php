<div class="card col-md-4 m-auto mt-4">
    <div class="card-header">
        Prueba de conocimientos
    </div>

    <div class="card-body">
        <small id="helpId" class="form-text text-muted">Digita la siguiente información opara ingresar al sistema</small>
        <form action="start.php" method="post" id="formulario">
            <div class="form-group">
              <label for="">Documento</label>
              <input type="text"
                class="form-control" name="documento" id="documento" placeholder="número de identificación">
              <!-- <small id="helpId" class="form-text text-muted">Help text</small> -->
            </div>
            <div class="form-group">
              <label for="">Clave</label>
              <input type="password"
                class="form-control" name="clave" id="clave" placeholder="*******">
              <!-- <small id="helpId" class="form-text text-muted">Help text</small> -->
            </div><br>
            <input type="hidden" name="accion" value="LOGIN">
            <button type="submit" name="" id="btnEnviar" class="btn btn-primary">Ingresar</button>
            <hr>
            <i class="text-lightgray">¿No tienes cuenta?, regístrate <a href="javascript:void()" onclick="cargarVista('register')">aquí</a></i>
        </form>
        <div class="respuesta"></div>
    </div>
</div>