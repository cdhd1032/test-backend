<?php

namespace Clase;

use Models\Peticion;

class CustomerData
{

    protected $ruta;
    protected $method;
    protected $nombre_peticion;
    protected $datos_post = [];
    public $data;

    /**
     * Class constructor.
     */
    public function __construct($method, $datos_post, $nombre_peticion)
    {
        $this->method = $method;
        $this->datos_post = $datos_post;
        $this->nombre_peticion = $nombre_peticion;

        $this->getData();
    }


    public function getData()
    {

        switch ($this->nombre_peticion) 
        {
            case 'USUARIOS':
                # code...
                $this->ruta = 'http://www.mocky.io/v2/5d9f38fd3000005b005246ac?mocky-delay=10s';
                break;

            case 'PAISES':
                # code...
                $this->ruta = 'https://restcountries.eu/rest/v2/all';
                break;

            default:
                # code...
                break;
        }

        $datos_post = json_encode(
            $this->datos_post
        );

        $opciones = array(
            'http' =>
            array(
                'method'  => $this->method,
                'header'  => 'Content-type: application/json',
                'content' => $datos_post
            )
        );

        $contexto = stream_context_create($opciones);

        $resultado = file_get_contents($this->ruta, false, $contexto);

        $this->data = ($resultado);

        $data = [
            "url" => $this->ruta,
            "respuesta" => $this->data,
            "nombre_peticion" => $this->nombre_peticion
        ];

        Peticion::create($data);

        return $this->data;

        // return $resultado;
    }
}
