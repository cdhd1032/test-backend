<?php

namespace Controllers;

use Helpers\ExisteUsuario;
use Models\Usuario;

class UsuarioController
{

    public static function createUser($data, $clave)
    {
        try {

            // Validar Formulario
            // Validar Requeridos
            if(!isset($data['nombre']) or strlen($data['nombre']) < 3){
                return ["ok" => "false", "msg" => "El nombre debe ser mínimo de 3 caracteres"];
            }

            if(!isset($data['documento']) or strlen($data['documento']) == 0){
                return ["ok" => "false", "msg" => "El documento es requerido"];
            }

            if(!isset($data['pais']) or strlen($data['pais']) == 0){
                return ["ok" => "false", "msg" => "El país es requerido"];
            }

            // Validar Email
            if (!filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                return ["ok" => "false", "msg" => "El email ({$data['email']}) no es válido."];
            }

            if(!isset($clave) or strlen($clave) < 6){
                return ["ok" => "false", "msg" => "La contraseña debe ser mínimo de 6 caracteres"];
            }

            // Validar si el correo o el documento existen
            $res = new ExisteUsuario($data['email'], $data['documento']);
            if($res->respuesta['ok']=='false'){
                return $res->respuesta;
            }

            // // Encriptar contraseña y asociarla al array que tiene la información a guardar
            $data["password"] = md5($clave);

            $user = Usuario::create($data);
            return $user;

        } catch (\Throwable $th) {
            //throw $th;
            $error = $th;
            return $error;
        }
    }


    public static function loginUser($documento, $clave)
    {
        try {

            // Validar si el correo o el documento existen
            $existeUsuario = Usuario::where('documento', $documento)
                                    ->where('password', md5($clave))
                                    ->get()
                                    ->toArray();
            
            if(!isset($existeUsuario[0]['documento'])){
                return ["ok" => "false", "msg" => "No encontramos el usuario o clave digitados"];
            }

            $_SESSION["global_usuario"] = $existeUsuario[0]['documento'];
            $_SESSION["global_nombre"] = $existeUsuario[0]['nombre'];
            $_SESSION["global_email"] = $existeUsuario[0]['email'];
            return ["ok" => "true", "msg" => "Sesión iniciada"];

        } catch (\Throwable $th) {
            //throw $th;
            $error = $th;
            return $error;
        }
    }




    public static function logoutUser()
    {
        try {

            session_unset();
            session_destroy();

            return ["ok" => "true", "msg" => "Sesión cerrada"];

        } catch (\Throwable $th) {
            //throw $th;
            $error = $th;
            return $error;
        }
    }
}
