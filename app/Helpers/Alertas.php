<?php

namespace Helpers;

class Alertas
{

    public function alertaDiv($tipo, $titulo, $mensaje)
    {
        return "<div class='alert alert-{$tipo} mt-3'><b>{$titulo}</b> {$mensaje}</div>";
    }
}
