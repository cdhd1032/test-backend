<?php

namespace Helpers;

use Models\Usuario;

class ExisteUsuario extends Usuario{

    protected $existeCorreo;
    protected $existeDocumento;
    public $respuesta;
    
    public function __construct($correo, $documento)
    {
        $this->existeDocumento = Usuario::where('documento', $documento)->count();
        $this->existeCorreo = Usuario::where('email', $correo)->count();

        if ($this->existeDocumento > 0) {
            $res = ["ok" => "false", "msg" => "El documento digitado ya existe"];
        }else if ($this->existeCorreo > 0) {
            $res = ["ok" => "false", "msg" => "El correo digitado ya existe"];
        }else{
            $res = ["ok" => "true", "msg" => "El correo digitado ya existe"];
        }

        $this->respuesta = $res;
    }
}