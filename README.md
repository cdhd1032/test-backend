# Pasos para la instalación

1. Clone o descargue el repositorio en un servidor
2. Abra un editor de linea de comandos y ejecute la linea composer install
3. Abra el proyecto en el navegador, lo redireccionará al archivo install.php
4. Digite la configuración de su gestor de base de datos mysql: Host, Nombre DB, Usuario, Clave
5. Guardé la configuración y siga los pasos del mensaje en pantalla
